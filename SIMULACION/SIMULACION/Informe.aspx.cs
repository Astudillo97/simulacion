﻿using Newtonsoft.Json;
using SIMULACION.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIMULACION
{
    public partial class Informe : System.Web.UI.Page
    {
        static List<Dato> listado = new List<Dato>();
        static Articulo articulo = null;
        static System.Data.DataTable data;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView1.DataSource = data;
                GridView1.DataBind(); 
            }
        }
        protected void metodoinsersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            RegistroDATOS.Visible = false;
            RegistroCSV.Visible = false;
            switch (metodoinsersion.SelectedValue)
            {

                case "datos":
                    RegistroDATOS.Visible = true;
                    break;
                case "csv":
                    RegistroCSV.Visible = true;
                    break;

                default:
                    procesarinformacion.Visible = false;
                    break;
            }
        }

        protected void AgregarDato_Click(object sender, EventArgs e)
        {
            try
            {
                listado.Add(new Dato() { orden = listado.Count, valor = Convert.ToDouble(valor.Text.Replace(".", ",")) });

                GridView1.DataSource = listado;
                GridView1.DataBind();

                valor.Text = "";
                error.Text = "";
            }
            catch (Exception)
            {
                error.Text = "Esta ingresando un dato no valido";
            }

        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex; 
            GridView1.DataSource = data;
            GridView1.DataBind(); 
        }
        protected void VisializarDatosCsv_Click(object sender, EventArgs e)
        {

        }

        protected void procesarinformacion_Click(object sender, EventArgs e)
        {

        }

        protected void Limpiar_Click(object sender, EventArgs e)
        {
            listado = new List<Dato>();
            GridView1.DataSource = listado;
            GridView1.DataBind();
        }

        protected void OrdenarMayoramenor_Click(object sender, EventArgs e)
        {
            listado = listado.OrderBy(lis => lis.valor).Reverse().ToList();
            GridView1.DataSource = listado;
            GridView1.DataBind();
        }

        protected void OrdenarMenoramayor_Click(object sender, EventArgs e)
        {
            listado = listado.OrderBy(lis => lis.valor).ToList();
            GridView1.DataSource = listado;
            GridView1.DataBind();
        }

        protected void CalcularValores_Click(object sender, EventArgs e)
        {
            try
            {
                listado = listado.OrderBy(lis => lis.valor).ToList();
                GridView1.DataSource = listado;
                GridView1.DataBind();

                Double Media = 0;
                for (int i = 0; i < listado.Count; i++)
                {
                    Media += listado[i].valor;
                }

                Media = Media / listado.Count;
                media_.Text = Media.ToString();
                var contador = listado.GroupBy(con => con.valor).Select(obj => new { valor = obj.Key, contador = obj.Count() });
                var var = contador.OrderBy(ord => ord.contador).Reverse().ToArray();
                moda_.Text = var[0].valor.ToString();
                if (listado.Count % 2 == 0)
                {
                    mediana_.Text = ((listado[(listado.Count - 1) / 2].valor + listado[(listado.Count) / 2].valor) / 2).ToString();
                }
                else
                {
                    mediana_.Text = (listado[Convert.ToInt32((listado.Count - 1) / 2)].valor).ToString();
                }

                double desv_esta = 0;
                foreach (var item in listado)
                {
                    desv_esta += Math.Pow((item.valor - Media), 2);
                }
                desv_esta = Math.Sqrt(desv_esta / (listado.Count - 1));
                desviacion_.Text = desv_esta.ToString();
            }
            catch (Exception ex)
            {
                error.Text = ex.Message;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToString() == "Eliminar")
                {
                    int index = Convert.ToInt32(e.CommandArgument);

                    listado.Remove(listado[index]);

                    GridView1.DataSource = data;
                    GridView1.DataBind();
                }
            }
            catch (Exception)
            {

            }
        }

        protected void Calcular_Individual_Click(object sender, EventArgs e)
        {
            try
            {
                articulo = new Articulo(ValidarNumero(costofijo),
                    ValidarNumero(costoproducion),
                    ValidarNumero(precioventanormal),
                    ValidarNumero(precioventaremate),
                    ValidarNumero(estimacionventa),
                    ValidarNumero(desviacion),
                    1);
                data = articulo.CalcularSimulacion();
                GridView1.DataSource = data;
                GridView1.DataBind();
            }
            catch (Exception)
            {

            }
        }
        protected void Calcular_iteraciones_Click(object sender, EventArgs e)
        {
            try
            {
                articulo = new Articulo(ValidarNumero(costofijo),
                    ValidarNumero(costoproducion),
                    ValidarNumero(precioventanormal),
                    ValidarNumero(precioventaremate),
                    ValidarNumero(estimacionventa),
                    ValidarNumero(desviacion),
                    ValidarNumero(muestras));
                data=articulo.CalcularSimulacion();
                GridView1.DataSource = data;
                GridView1.DataBind();
            }
            catch (Exception)
            {

            }
        }
        public double ValidarNumero(TextBox entrada)
        {
            try
            {
                if (entrada.Text != "")
                {
                    entrada.BorderColor = System.Drawing.Color.Green;
                    return Convert.ToDouble(entrada.Text);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {
                entrada.BorderColor = System.Drawing.Color.Red;
                return 0;
            }

        }

        protected void Histograma_Click(object sender, EventArgs e)
        {
            var jsonString = JsonConvert.SerializeObject(data);
            ClientScript.RegisterStartupScript(GetType(), "myscript", "VerHistograma("+ jsonString + ");", true);
        }
    }
}