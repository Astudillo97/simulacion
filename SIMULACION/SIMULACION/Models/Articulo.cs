﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SIMULACION.Models
{
    public class Articulo
    {
        public double costofijo { get; set; }
        public double costoproducion { get; set; }
        public double precioventanormal { get; set; }
        public double precioventaremate { get; set; }
        public double esperanzadeventa { get; set; }
        public double desviaciondeestandar { get; set; }
        public double iteraciones { get; set; }

        public Articulo(double costofijo, double costoproducion, 
            double precioventanormal, double precioventaremate, 
            double esperanzadeventa, double desviaciondeestandar,
            double iteraciones)
        {
            this.costofijo = costofijo;
            this.costoproducion = costoproducion;
            this.precioventanormal = precioventanormal;
            this.precioventaremate = precioventaremate;
            this.esperanzadeventa = esperanzadeventa;
            this.desviaciondeestandar = desviaciondeestandar;
            this.iteraciones = iteraciones;
        }
        public double CalcularCostoProducion()
        {
            return (esperanzadeventa * costoproducion);
        }
        public double CalcularVentaNormal(double cantidadvendidas)
        {
            return cantidadvendidas<esperanzadeventa? (cantidadvendidas*precioventanormal):(esperanzadeventa* precioventanormal);
        }
        public double CalcularVentaRemate(double cantidadvendidas)
        {
            return cantidadvendidas < esperanzadeventa ? ((esperanzadeventa- cantidadvendidas) * precioventaremate) : 0;
        }
        public double CalcularCostoTotal()
        {
            return CalcularCostoProducion() + costofijo;
        }
        public double CalcularVentasTotales(double cantidadvendidas)
        {
            return CalcularVentaNormal(cantidadvendidas) + CalcularVentaRemate(cantidadvendidas);
        }
        public double CalcularUtilidad(double cantidadvendidas)
        {
            return (CalcularVentasTotales(cantidadvendidas)-CalcularCostoTotal());
        }
        public DataTable CalcularSimulacion()
        {
            List<double> simulacion = new List<double>();
            DataTable listado = new DataTable();
            listado.Columns.AddRange(new DataColumn[] {
            new DataColumn("ESTIMADO"),
            new DataColumn("ALEATORIO"),
            new DataColumn("COSTOPRODUCION"),
            new DataColumn("VALORESVENTAS"),
            new DataColumn("UTILIDAD"),
            });
            Aleatorio aleatorio = new Aleatorio();
            for (int i = 1; i <= iteraciones; i++)
            {
                DataRow temporal = listado.NewRow();
                double aleatoriotemporal = aleatorio.CongruencialMixto(Convert.ToInt32(esperanzadeventa - desviaciondeestandar), Convert.ToInt32(esperanzadeventa + desviaciondeestandar));
                temporal["ESTIMADO"] = this.esperanzadeventa;
                temporal["ALEATORIO"] = aleatoriotemporal;
                temporal["COSTOPRODUCION"] = CalcularCostoTotal(); 
                temporal["VALORESVENTAS"] = CalcularVentasTotales(aleatoriotemporal);
                temporal["UTILIDAD"] = CalcularUtilidad(aleatoriotemporal);
                listado.Rows.Add(temporal);
            }
            return listado;
        }
    }
}