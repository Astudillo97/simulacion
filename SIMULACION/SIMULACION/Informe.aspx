﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Informe.aspx.cs" Inherits="SIMULACION.Informe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }
    </style>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-md-4">
                    <div class="">
                        <asp:DropDownList runat="server" ID="metodoinsersion" OnSelectedIndexChanged="metodoinsersion_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                            <asp:ListItem Text="Seleccione" Value="Seleccione" />
                            <asp:ListItem Text="DATOS" Value="datos" />
                            <asp:ListItem Text="CSV" Value="csv" />
                        </asp:DropDownList>
                        <fieldset visible="false" runat="server" id="RegistroDATOS">
                            <legend>Registro por Datos</legend>
                            <asp:TextBox runat="server" ID="valor" />
                            <asp:Label Text="" ID="error" runat="server" />
                            <asp:Button Text="Agregar" ID="AgregarDato" OnClick="AgregarDato_Click" runat="server" />
                        </fieldset>
                        <fieldset visible="false" runat="server" id="RegistroCSV">
                            <legend>Registro por CSV</legend>
                            <asp:FileUpload runat="server" ID="archivo" />
                            <p>Recuerde que es un archivo delimitado por ; </p>
                            <asp:Button Text="Visializar" ID="VisializarDatosCsv" OnClick="VisializarDatosCsv_Click" runat="server" />
                        </fieldset>
                    </div>
                    <div>
                        Costo Fijo:<asp:TextBox runat="server" ID="costofijo" />
                        Costo de producion:<asp:TextBox runat="server" ID="costoproducion" />
                        Precio de venta(Normal):<asp:TextBox runat="server" ID="precioventanormal" />
                        Precio de venta(Remate):<asp:TextBox runat="server" ID="precioventaremate" />
                        Esperanza de unidades a vender:<asp:TextBox runat="server" ID="estimacionventa" />
                        Desviación estandar:<asp:TextBox runat="server" ID="desviacion" />
                        
                        <asp:Button Text="Calcular individual" ID="Calcular_Individual" OnClick="Calcular_Individual_Click" runat="server" />
                        <hr />
                        Iteraciones: <asp:TextBox runat="server" ID="muestras" />
                        <asp:Button Text="Calcular iteraciones" ID="Calcular_iteraciones" OnClick="Calcular_iteraciones_Click" runat="server" />
                        
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="box">
                        <div class="box-header">
                            <div class="title">Vista de datos </div>
                            <asp:Button Text="Limpiar" OnClick="Limpiar_Click" ID="Limpiar" CssClass="btn btn-link" runat="server" />
                            <asp:Button Text="Ordenar de Mayor a menor" OnClick="OrdenarMayoramenor_Click" ID="OrdenarMayoramenor" CssClass="btn btn-link" runat="server" />
                            <asp:Button Text="Ordenar de Menor a Mayor" OnClick="OrdenarMenoramayor_Click" ID="OrdenarMenoramayor" CssClass="btn btn-link" runat="server" />
                            <asp:Button Text="Calcalar" OnClick="CalcularValores_Click" ID="CalcularValores" CssClass="btn btn-link" runat="server" />
                        </div>
                        <div class="box-body">

                            <asp:GridView ID="GridView1" OnRowCommand="GridView1_RowCommand" CssClass="table table-striped table-hover" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                OnPageIndexChanging="OnPageIndexChanging" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                                <AlternatingRowStyle BackColor="#DCDCDC" />
                                <Columns>
                                    <asp:BoundField DataField="ESTIMADO" HeaderText="ESTIMADO"></asp:BoundField>
                                    <asp:BoundField DataField="ALEATORIO" HeaderText="ALEATORIO"></asp:BoundField>
                                    <asp:BoundField DataField="COSTOPRODUCION" HeaderText="COSTOPRODUCION"></asp:BoundField>
                                    <asp:BoundField DataField="VALORESVENTAS" HeaderText="VALORESVENTAS"></asp:BoundField>
                                    <asp:BoundField DataField="UTILIDAD" HeaderText="UTILIDAD"></asp:BoundField>
                                    <asp:ButtonField ButtonType="Link" CommandName="Eliminar" Text="" ControlStyle-CssClass=" glyphicon glyphicon-remove-circle" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                <PagerStyle CssClass="pagination-ys" BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#000065" />
                            </asp:GridView>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-3">
                                    MEDIA
                                    <asp:Label Text="" ID="media_" runat="server" />
                                </div>
                                <div class="col-md-3">
                                    MODA
                                    <asp:Label Text="" ID="moda_" runat="server" />
                                </div>
                                <div class="col-md-3">
                                    MEDIANA
                                    <asp:Label Text="" ID="mediana_" runat="server" />
                                </div>
                                <div class="col-md-3">
                                    DESVIACION
                                    <asp:Label Text="" ID="desviacion_" runat="server" />
                                </div>
                            </div>
                            <asp:Button Text="Procesar Información" Visible="false" ID="procesarinformacion" OnClick="procesarinformacion_Click" runat="server" />
                        </div>
                    </div>


                </div>
            </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button Text="Ver Histograma" ID="Histograma" runat="server" OnClick="Histograma_Click"  />
    <div class="box">
                <div id='myDiv'>
                    <!-- Plotly chart will be drawn inside this DIV -->
                </div>
        <div id='myDiv1'>
                    <!-- Plotly chart will be drawn inside this DIV -->
                </div>
            </div>
            <script src='https://cdn.plot.ly/plotly-latest.min.js'></script>

            <script>
                function VerHistograma(mydata) {
                    if (mydata!=null) {
                        var x = [];
                        for (var i = 0; i < mydata.length; i++) {
                            x[i] = mydata[i].UTILIDAD;
                        }
                        var trace = {
                            x: x,
                            type: 'histogram',
                        };
                        var data = [trace];
                        Plotly.newPlot('myDiv', data);


                        var trace1 = {
                            x: x,
                            type: 'histogram',
                            cumulative: { enabled: true }
                        };
                        var data1 = [trace1];
                        Plotly.newPlot('myDiv1', data1);
                    }
                    
                }
                
            </script>
</asp:Content>
